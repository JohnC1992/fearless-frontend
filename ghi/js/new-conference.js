
window.addEventListener("DOMContentLoaded", async ()=> {
    const url = "http://localhost:8000/api/locations/";

    try {
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            const option_list = document.querySelector("select")
            for (const location of data.locations){
                option_list.innerHTML += `<option value = '${location.id}'>
                    ${location.name}</option>`;
            }
        }
        else{
            //bad response
        }

    }
    catch(e){

    }
    try{
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            console.log(json)
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
                console.log(newConference);
            }
        });
    }catch(e){

    }

})
